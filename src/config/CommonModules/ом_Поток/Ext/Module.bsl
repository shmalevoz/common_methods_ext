﻿// Поток выполнения - интерфейс к абстрактной (параллельно) выполняемой 
// 	длительной операции. Обычно является составной частью <a href="./ом_Процесс.html">процесса</a>
// Подробности в <a href="./tutorial-Многозадачность.html">руководстве</a>
// 
// Модуль оперирует описанием (ссылкой) потока. Для модификации потока / использования
// 	видозависмого функционала потока необходимо получить <code>Объект()</code> потока
// 
// Идентификатор потока - Строка - составное значение из <Идентификатор>#<Вид потока>
// 
// Допустимо использование и в клиентском и в серверном контекстах. При попытке 
// 	получения объекта потока в неподходящем для использования вида контексте выполнения 
// 	будет сгенерировано Исключение
// 
// Кратко использование
// <code>
// 	// Группирующий потоки процесс
// 	Процесс	= ом_Процесс.Создать("Обновление справочника");
// 	// Набор обновляемых элементов справочника, разбитый на части
// 	СсылкиНабор	= Новый Массив();
// 	...
// 	Для Каждого ПотокСсылки Из СсылкиНабор Цикл
// 		// Новый объект потока
// 		Поток	= ом_Поток.Создать(Процесс, ом_ФоновоеЗадание.ПотокВид());
// 		// методы, спецмфичные для объекта вида потока
// 		Поток.Параметры().Добавить(ПотокСсылки);
// 		Поток.Метод("Обработка.ОбновлениеСправочника.Обновить");
// 		ом_Поток.Запустить(Поток);
// 	КонецЦикла;
// </code>
// 

#Область ПрограммныйИнтерфейс

// Возвращает новое описание потока
//
// Параметры: 
// 	Процесс - Структура - Описание процесса-владельца потока
// 	Вид - Строка - Вид потока
//
// Возвращаемое значение: 
// 	Структура
//
Функция Создать(Вид) Экспорт
	
	Результат	= Новый Структура();
	
	Вид(Результат, Вид);
	
	Возврат Результат;
	
КонецФункции // Создать 

// Устанавливает поле Вид описания потока. Возвращает значение на момент
// 	до выполнения метода
//
// Параметры: 
// 	Поток - Структура - Описание поттока
// 	Значение - Строка - Новое значение
//
// Возвращаемое значение: 
// 	Строка
//
Функция Вид(Поток, Значение = Null) Экспорт
	
	Возврат ом_Коллекция.СтруктураСвойство(Поток, ИмяВид(), Значение);
	
КонецФункции // Вид 

// Устанавливает поле Наименование описания потока. Возвращает значение на момент 
// 	до выполнения метода
//
// Параметры: 
// 	Поток - Структура - Описание потока
// 	Значение - Строка - новое значение
//
// Возвращаемое значение: 
// 	Строка
//
Функция Наименование(Поток, Значение = Null) Экспорт
	
	Возврат ом_Коллекция.СтруктураСвойство(Поток, ИмяНаименование(), Значение);
	
КонецФункции // Наименование 




//// Возвращает идентификатор созданного фонового задания
////
//// Параметры:  
//// 	Метод - Строка - Полное имя вызываемого метода вида Объект.Метод
//// 	Параметры - Массив - Массив параметров вызываемого метода
//// 	Ключ - Строка - Ключ уникальности задания
//// 	Данные - ДвоичныеДанные - Файл внешней обработки (при ее использовании)
//// 	Группа - Структура - Настройки группы потоков, см. ом_ПотокГруппа
////
//// Возвращаемое значение: 
//// 	УникальныйИдентификатор
////
//// BSLLS:NumberOfOptionalParams-off - все необходимы
//Функция Создать(Метод
//	, Параметры = Null
//	, Ключ = Null
//	, Данные = Null
//	, Группа = Null) Экспорт
//	
//	Возврат ом_ПотокВызовСервера.Создать(Метод, Параметры, Ключ, Данные, Группа);
//	
//// BSLLS:NumberOfOptionalParams-on
//КонецФункции // Создать 

//// Возвращает объект фонового задания
////
//// Параметры: 
//// 	Идентификатор - УникальныйИдентификатор - Идентификатор потока
////
//// Возвращаемое значение: 
//// 	ФоновоеЗадание, Неопределено
////
//Функция Получить(Идентификатор) Экспорт
//	
//	Возврат ФоновыеЗадания.НайтиПоУникальномуИдентификатору(Идентификатор);
//	
//КонецФункции // Получить 

//// Возвращает идентификатор потока
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	УникальныйИдентификатор
////
//Функция Идентификатор(Поток) Экспорт
//	
//	Возврат Поток.УникальныйИдентификатор;
//	
//КонецФункции // Идентификатор 

//// Возвращает массив потоков по отбору. Необходимо помнить, что
//// 	имена потоков, созданных библиотекой, формируются по правилам. 
//// 	[см. описание модуля](#top)
////
//// Параметры: 
//// 	Отбор - Структура - см. ПолучитьФоновыеЗадания
////
//// Возвращаемое значение: 
//// 	Массив                                ё
////
//Функция Набор(Отбор = Null) Экспорт
//	
//	Результат	= Новый Массив();
//	
//	// Помним, что все фоновые задания проксируются через вызов
//	// ом_ПотокВызовСервера.МетодыВыполнить, а имя
//	// конечного метода дописывается в ключ задания
//	МетодИмя	= Неопределено;
//	// BSLLS:Typo-off - ложное срабатывание
//	МетодКлюч	= "имяметода";
//	КлючИмя		= "ключ";
//	// BSLLS:Typo-on
//	Поиск		= Новый Структура();
//	ПоискКлюч	= Неопределено;
//	
//	Если Отбор <> Null Тогда
//		Для Каждого Элемент Из Отбор Цикл
//			Ключ		= Элемент.Ключ;
//			Значение	= Элемент.Значение;
//			Если НРег(Ключ) = МетодКлюч Тогда
//				МетодИмя	= НРег(Элемент.Значение);
//				Значение	= "ом_ПотокВызовСервера.МетодВыполнить";
//			ИначеЕсли НРег(Ключ) <> КлючИмя Тогда
//				Поиск.Вставить(Ключ, Значение);
//			Иначе
//				ПоискКлюч	= Значение;
//			КонецЕсли;
//		КонецЦикла;
//	КонецЕсли;
//	
//	КлючДлина	= ?(ПоискКлюч = Неопределено, 0, СтрДлина(ПоискКлюч));
//	Для Каждого Процесс Из ФоновыеЗадания.ПолучитьФоновыеЗадания(Поиск) Цикл
//		Позиция	= СтрНайти(Процесс.Ключ, "#");
//		// BSLLS:IfConditionComplexity-off - тривиальное условие
//		Если (МетодИмя = Неопределено
//			ИЛИ (Позиция И Лев(НРег(Процесс.Ключ), Позиция - 1) = МетодИмя))
//			И (ПоискКлюч = Неопределено ИЛИ Прав(Процесс.Ключ, КлючДлина) = ПоискКлюч)
//			Тогда
//			Результат.Добавить(Процесс);
//		КонецЕсли;
//		// BSLLS:IfConditionComplexity-on
//	КонецЦикла;
//	
//	Возврат Результат;
//	
//КонецФункции // Набор 

//// Истина, если задание с указанным методом активно
////
//// Параметры:  
//// 	Метод - Строка - Полное имя метода задания
////
//// Возвращаемое значение: 
//// 	Булево
////
//Функция МетодВыполняется(Метод) Экспорт
//	
//	Возврат Набор(Новый Структура("Состояние, ИмяМетода"
//	, СостояниеФоновогоЗадания.Активно
//	, Метод)).Количество() <> 0;
//	
//КонецФункции // МетодВыполняется 

//// Возвращает состояние потока
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	СостояниеФоновогоЗадания
////
//Функция Статус(Поток) Экспорт
//	
//	Возврат Поток.Состояние;
//	
//КонецФункции // Статус 

//// Истина, если поток активен
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	Булево
////
//Функция Активен(Поток) Экспорт
//	
//	Возврат Статус(Поток) = СостояниеФоновогоЗадания.Активно;
//	
//КонецФункции // Активен 

//// Истина, если поток завершен
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	Булево
////
//Функция Завершен(Поток) Экспорт
//	
//	Возврат Статус(Поток) = СостояниеФоновогоЗадания.Завершено;
//	
//КонецФункции // Завершен 

//// Истина, если поток завершен аварийно
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	Булево
////
//Функция ЗавершенАварийно(Поток) Экспорт
//	
//	Возврат Статус(Поток) = СостояниеФоновогоЗадания.ЗавершеноАварийно;
//	
//КонецФункции // ЗавершенАварийно 

//// Истина, если выполнение потока отменено
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	Булево
////
//Функция Отменен(Поток) Экспорт
//	
//	Возврат Статус(Поток) = СостояниеФоновогоЗадания.Отменено;
//	
//КонецФункции // Отменен 

//// Прерывает поток выполнения
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Прерываемый поток
////
//Процедура Отменить(Поток) Экспорт
//	
//	Поток.Отменить();
//	
//КонецПроцедуры // Отменить 

//// Ожидает завершение выполнения потока до тайаута. Возвращает обновленный объект потока
//// 	ВНИМАНИЕ! Для платформы 8.3.12 и ниже при аварийном завершении потока присутствует скрытое исключение
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Поток выполнения
//// 	Таймаут - Число - Количество секунд ожидания
////
//// Возвращаемое значение: 
//// 	ФоновоеЗадание - Обновленыый объект потока
////
//Функция ЗавершениеОжидать(Поток, Таймаут = Null) Экспорт
//	
//	Результат	= Неопределено;
//	
//	Если ом_СистемаИнформация.ПриложениеВерсия8_3_13Есть() Тогда
//		Если Таймаут = Null Тогда
//			Результат	= Поток.ОжидатьЗавершениеВыполнения();
//		Иначе
//			Результат	= Поток.ОжидатьЗавершенияВыполнения(Таймаут);
//		КонецЕсли;
//	Иначе
//		Идентификатор	= Идентификатор(Поток);
//		Попытка
//			Если Таймаут = Null Тогда
//				Поток.ОжидатьЗавершения();
//			Иначе
//				Поток.ОжидатьЗавершения(Таймаут);
//			КонецЕсли;
//		Исключение
//			ЗаписьЖурналаРегистрации("ом_Поток.ЗавершениеОжидать"
//			, УровеньЖурналаРегистрации.Ошибка
//			, 
//			, 
//			, "Исключение при ожидании завершения потока " + Идентификатор);
//		КонецПопытки;
//		Результат	= Получить(Идентификатор);
//	КонецЕсли;
//	
//	Возврат Результат;
//	
//КонецФункции // ЗавершениеОжидать 

//// Возвращает дату/время начала выполнения потока
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	Дата
////
//Функция Начало(Поток) Экспорт
//	
//	Возврат Поток.Начало;
//	
//КонецФункции // Начало 

//// Возвращает дату/время окончания выполнения потока
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	Дата
////
//Функция Окончание(Поток) Экспорт
//	
//	Возврат Поток.Конец;
//	
//КонецФункции // Окончание 

//// Возвращает информацию об ошибке выполнения потока
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	ИнформацияОбОшибке, Неопределено
////
//Функция ОшибкаИнформация(Поток) Экспорт
//	
//	Возврат Поток.ИнформацияОбОшибке;
//	
//КонецФункции // ОшибкаИнформация 

//// Возвращает регламентное задание запустившее поток
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	РегламентноеЗадание
////
//Функция РегламентноеЗадание(Поток) Экспорт
//	
//	Возврат Поток.РегламентноеЗадание;
//	
//КонецФункции // РегламентноеЗадание 

//// Возвращает адрес сервера, на котором выполняется поток
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
////
//// Возвращаемое значение: 
//// 	ФоновоеЗадание
////
//Функция Расположение(Поток) Экспорт
//	
//	Возврат Поток.Расположение;
//	
//КонецФункции // Расположение 

//// Возвращает сообщения пользоватею, опубликованные потоком
////
//// Параметры: 
//// 	Поток - ФоновоеЗадание - Исследуемый поток
//// 	Удалить - Булево - Флаг удаления сообщений
////
//// Возвращаемое значение: 
//// 	ФиксированныйМассив.<СообщениеПользователю>
////
//Функция Сообщения(Поток, Удалить = Ложь) Экспорт
//	
//	Возврат Поток.ПолучитьСообщенияПользователю(Удалить);
//	
//КонецФункции // Сообщения 

//// Возвращает ключ ключ уникальности задания
////
//// Параметры: 
//// 	Метод - Строка - Полное имя выполняемого метода
//// 	Уникальность - Строка - Базовый ключ уникальности задания
////
//// Возвращаемое значение: 
//// 	Строка
////
//Функция МетодКлюч(Метод, Уникальность) Экспорт
//	
//	Возврат Метод + "#" + Уникальность;
//	
//КонецФункции // МетодКлюч 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Возвращает имя Вид
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИмяВид()
	
	Возврат "Вид";
	
КонецФункции // ИмяВид 

// Возвращает имя Наименование
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ИмяНаименование()
	
	Возврат "Наименование";
	
КонецФункции // ИмяНаименование 

#КонецОбласти
