﻿// Кэш к информации о метаданных
//  

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает коллекцию известных классов метаданных
// 
// Параметры: 
// 	Регион - Строка, Ссылка - Регион использования адаптера
// 
// Возвращаемое значение: 
// 	ФиксированнаяСтруктура - Коллекция доступных классов метаданных
// 
Функция КлассыКоллекция(Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.КлассыКоллекция(Регион);
	
КонецФункции // КлассыКоллекция 

// Возвращает набор картинок отображения классов
//
// Параметры: 
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Соответствие - Ключ имя класса, значение картинка
//
Функция КлассыКартинки(Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.КлассыКартинки(Регион);
	
КонецФункции // КлассыКартинки 

// Возвращает структуру состава метаданных
// 
// Параметры: 
// 	Регион - Строка, Ссылка - Регион использования адаптера
// 
// Возвращаемое значение: 
// 	ФиксированнаяСтруктура - Состав метаданных
// 
Функция Состав(Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.Состав(Регион);
	
КонецФункции // Состав 

// Возвращает состав объектов метаданных указанного класса
// 
// Параметры: 
// 	Класс - Строка - Имя класса объектов (Справочник, Документ, ...)
// 	Регион - Строка, Ссылка - Регион использования адаптеров
// 
// Возвращаемое значение: 
// 	ФиксированнаяСтруктура - Состав метаданных заданного класса
// 
Функция КлассСостав(Класс, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.КлассСостав(Класс, Регион);
	
КонецФункции // КлассСостав 

// Вовращает менеджер объектов метаданных указанного класса
// 
// Параметры: 
// 	Класс - Строка - Имя класса объектов (Справочник, Документ, ...)
// 	Регион - Строка, Ссылка - Регион использования адаптеров
// 
// Возвращаемое значение: 
// 	ОбъектМетаданных, Неопределено - Менеджер объектов указанного класса
// 
Функция КлассМенеджер(Класс, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.КлассМенеджер(Класс, Регион);
	
КонецФункции // КлассМенеджер 

// Возвращает состав метаданных по имени и уточнению типа
// 
// Параметры: 
// 	ТипИмя - Строка - Имя типа
// 	Уточнение - Строка - Уточнение типа
// 	Регион - Строка, Ссылка - Регион использования адаптеров
// 
// Возвращаемое значение: 
// 	Структура - Состав метаданных типа
// 
Функция ОбъектСостав(ТипИмя, Уточнение = Неопределено, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.ОбъектСостав(ТипИмя, Уточнение, Регион);
	
КонецФункции // ОбъектСостав 

// Истина, если для типа определен реквизит шапки
// 
// Параметры: 
// 	ТипИмя - Строка - Имя типа
// 	Реквизит - Строка - Имя реквизита по метаданным
// 	Уточнение - Произвольный - Уточнение типа
// 	Регион - Строка, Ссылка - Регион использования адаптеров
// 
// Возвращаемое значение: 
// 	Булево
// 
Функция ШапкаРеквизитЕсть(ТипИмя, Реквизит, Уточнение = Неопределено, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.ШапкаРеквизитЕсть(ТипИмя, Реквизит, Уточнение, Регион);
	
КонецФункции // ШапкаРеквизитЕсть 

// Истина, если для типа определен реквизит табличной части
// 
// Параметры: 
// 	ТипИмя - Строка - Имя типа
// 	ТабличнаяЧасть - Строка - Имя табличной части по метаданным
// 	Реквизит - Строка - Имя реквизита по метаданным
// 	Уточнение - Произвольный - Уточнение типа
// 	Регион - Строка, Ссылка - Регион использования адаптеров
// 
// Возвращаемое значение: 
// 	Булево
// 
Функция ТабличнаяЧастьРеквизитЕсть(ТипИмя, ТабличнаяЧасть, Реквизит
	, Уточнение = Неопределено, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.ТабличнаяЧастьРеквизитЕсть(ТипИмя
	, ТабличнаяЧасть
	, Реквизит
	, Уточнение
	, Регион);
	
КонецФункции // ТабличнаяЧастьРеквизитЕсть 

// Возвращает описание типов любой точки маршрута
// 
// Параметры:  
// 
// Возвращаемое значение: 
// 	ОписаниеТипов
// 
Функция ТипОписаниеТочкаМаршрутаПроизвольная() Экспорт
	
	// Тип строим напрямую по метаданным, без использования адаптера 
	// поскольку остальные типы строятся аналогичным способом
	
	ТипыМассив	= Новый Массив;
	
	Для Каждого Процесс Из Метаданные.БизнесПроцессы Цикл
		ТипыМассив.Добавить(Тип("ТочкаМаршрутаБизнесПроцессаСсылка." + Процесс.Имя));
	КонецЦикла;
	
	Возврат Новый ОписаниеТипов(ТипыМассив);
	
КонецФункции // ТипОписаниеТочкаМаршрутаПроизвольная 

// Возвращает имя реквизита уточнения по имени типа
// 
// Параметры: 
// 	ТипИмя - Строка - Полное имя типа
// 	Регион - Строка, Ссылка - Регион использования адаптеров
// 
// Возвращаемое значение: 
// 	Строка - Имя реквизита содержащего уточнение типа
// 
Функция УточнениеРеквизитИмя(ТипИмя, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.УточнениеРеквизитИмя(ТипИмя, Регион);
	
КонецФункции // УточнениеРеквизитИмя 

// Возвращает массив возможных уточнений для имени типа
//
// Параметры: 
// 	ТипИмя - Строка - Полное имя типа
// 	Регион - Строка, Ссылка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Массив
//
Функция УточненияМассив(ТипИмя, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.УточненияМассив(ТипИмя, Регион);
	
КонецФункции // УточненияМассив 

// Возвращает состав типов подсистемы
//
// Параметры: 
// 	ТипИмя - Строка - Полное имя подсистемы
// 	Подчиненные - Булево - Флаг включения подчиненных подсистем
// 	Уточнение - Произвольный - Уточнение типа
// 	Регион - Строка, Ссылка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	ФиксированныйМассив
//
Функция ПодсистемаСостав(ТипИмя, Подчиненные = Ложь, Уточнение = Неопределено, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.ПодсистемаСостав(ТипИмя, Подчиненные, Уточнение, Регион);
	
КонецФункции // ПодсистемаСостав 

// Возвращает коллекцию возможных режимов совместимости
//
// Параметры: 
// 	Регион - Строка, Ссылка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Соответствие - Ключ значение режима, Значение представление режима
//
Функция СовместимостьРежимыКоллекция(Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.СовместимостьРежимыКоллекция(Регион);
	
КонецФункции // СовместимостьРежимыКоллекция 

// Возвращает строку версии совместимости
//
// Параметры: 
// 	Регион - Строка, Ссылка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Строка
//
Функция СовместимостьВерсия(Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.СовместимостьВерсия(Регион);
	
КонецФункции // СовместимостьВерсия 

// Истина, если разрешены модальные окна
//
// Параметры: 
// 	ПредупрежденияВозможны - Булево - Флаг допустимости предупреждений
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Булево
//
Функция ОкнаМодальныеРазрешены(ПредупрежденияВозможны = Ложь, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.ОкнаМодальныеРазрешены(ПредупрежденияВозможны, Регион);
	
КонецФункции // ОкнаМодальныеРазрешены 

// Истина, если доступен РежимИспользованияМодальности
//
// Параметры: 
// 	Регион - Строка, Ссылка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Булево
//
Функция РежимИспользованияМодальностиЕсть(Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.РежимИспользованияМодальностиЕсть(Регион);
	
КонецФункции // РежимИспользованияМодальностиЕсть 

// Истина, если разрешены синхронные вызовы
//
// Параметры: 
// 	ПредупрежденияВозможны - Булево - Флаг допустимости с предупреждениями
// 	Регион - Строка, Ссылка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Булево
//
Функция ВызовыСинхронныеРазрешены(ПредупрежденияВозможны = Ложь, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.ВызовыСинхронныеРазрешены(ПредупрежденияВозможны, Регион);
	
КонецФункции // ВызовыСинхронныеРазрешены 

// Истина, если есть свойство режима использования синхронных вызовов
//
// Параметры: 
// 	Регион - Строка, Ссылка - Регион использования адаптеров
//
// Возвращаемое значение: 
//
Функция РежимИспользованияСинхронныхВызововЕсть(Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.РежимИспользованияСинхронныхВызововЕсть(Регион);
	
КонецФункции // РежимИспользованияСинхронныхВызововЕсть 

// Истина, если форма управляемая
//
// Параметры: 
// 	Имя - Строка - Полное имя формы
// 	Регион - Строка, Ссылка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Булево
//
Функция ФормаУправляемаяЭто(Имя, Регион = Неопределено) Экспорт
	
	Возврат ом_МетаданныеАдаптер.ФормаУправляемаяЭто(Имя, Регион);
	
КонецФункции // ФормаУправляемаяЭто 

// Возвращает активный адаптер к метаданым
// 
// Параметры: 
// 	Регион - Строка, Ссылка - Регион использования адаптеров
// 
// Возвращаемое значение: 
// 	ОбработкаОбъект - Адаптер к метаданным
// 
Функция Адаптер(Регион = Неопределено) Экспорт
	
	Возврат ом_АдаптерАктивный.Объект(ом_МетаданныеСловарь.АдаптерВид(), Регион);
	
КонецФункции // Адаптер 

#КонецОбласти
